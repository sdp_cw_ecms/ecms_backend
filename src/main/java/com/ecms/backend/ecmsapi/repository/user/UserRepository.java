package com.ecms.backend.ecmsapi.repository.user;
import com.ecms.backend.ecmsapi.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  /**
   * Retrieves an optional user by their username.
   *
   * @param username the username of the user to retrieve
   * @return an optional containing the user if found, otherwise an empty optional
   */
  Optional<User> findByUsername(String username);

  /**
   * Checks if a user exists with the specified username.
   *
   * @param username the username to check
   * @return true if a user exists with the given username, false otherwise
   */
  Boolean existsByUsername(String username);

  /**
   * Checks if a user exists with the specified email.
   *
   * @param email the email to check
   * @return true if a user exists with the given email, false otherwise
   */
  Boolean existsByEmail(String email);

}
